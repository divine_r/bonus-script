import import l2open.gameserver.Announcements;
// Показывает текст игроку на экране
private static void enoughtItem(L2Player player, int itemid, long count)
	{
		player.sendPacket(new ExShowScreenMessage(new CustomMessage("util.enoughItemCount", player).addString(formatPay(player, count, itemid)).toString(), 5000, ScreenMessageAlign.TOP_CENTER, true, 1, -1, false));
		player.sendMessage(new CustomMessage("util.enoughItemCount", player).addString(formatPay(player, count, itemid)));
	}
    
//Анонсит начало ЦТФ
Announcements.getInstance().announceByCustomMessage("scripts.events.CtF.AnnounceEventStarted", null);